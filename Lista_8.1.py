"""
Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
followed by one integer c.
Multiply every element by c and print the result.
"""

# Read a 2D list of integers:
D=input().split()
NUM_ROWS=int(D[0])
NUM_COLS=int(D[1])
#NUM_COLS=4
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
#print(a)
c=int(input())

for ci in range(NUM_ROWS):
  for cj in range(NUM_COLS):
    a[ci][cj]=a[ci][cj]*c

for fila in a:
  for columna in fila:
    print(columna, end=" ")
  print()