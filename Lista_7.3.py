"""
Given a list of numbers, find and print all its elements that are greater than their left neighbor.

"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)

for i in range(1, len(a)):
    if a[i] > a[i-1]:
        print(a[i], end=' ')