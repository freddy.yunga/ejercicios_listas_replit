"""
Given a list of distinct numbers, swap the minimum and the maximum and print the resulting list.
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
#print(a)

minimo=min(a)
i1=a.index(minimo)
maximo=max(a)
i2=a.index(maximo)
#print(minimo,maximo)
a[i1]=maximo
a[i2]=minimo
print(a)