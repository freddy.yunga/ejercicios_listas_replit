"""
Given a list of numbers, count a number of pairs of equal elements.
Any two elements that are equal to each other should be counted exactly once.
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
"""
cont=0
cont2=0
for s in a:
  if a[s]==a[s+1]:
    cont=cont+1
  else:
    cont2=cont2+1
print(cont)
"""
n = len(a)
c = 0
for i in range(0, n):
    for j in range(i + 1, n):
        if (a[i] == a[j]):
            c += 1
print(c)
