"""
Given an odd positive integer n, produce a two-dimensional array of size n×n.
Fill each element with the character "." .
Then fill the middle row, the middle column and the diagonals with the character "*".
You'll get an image of a snow flake.
Print the snow flake in n×n rows and columns and separate the characters with a single space.

"""

# Read an integer:
a = int(input())
# Print a value:
# print(a)

b = [['.'] * a for i in range(a)]
for i in range(a):
  b[i][i] = '*'
  b[a // 2][i] = '*'
  b[i][a // 2] = '*'
  b[i][a - i - 1] = '*'
for row in b:
  print(' '.join(row))