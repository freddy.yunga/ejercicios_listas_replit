"""
Given an integer n, create a two-dimensional array of size n×n according to the following rules and print it:

On the antidiagonal put 1.
On the diagonals above it put 0.
On the diagonals below it put 2.
"""

