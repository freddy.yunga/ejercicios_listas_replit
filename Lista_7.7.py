"""
Given a list of numbers, swap adjacent elements in each pair (swap A[0] with A[1], A[2] with A[3], etc.).
Print the resulting list. If a list has an odd number of elements, leave the last element intact.
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
"""
for s in a:
  a[0]=a[1]
  print(s, end=" ")
"""
for i in range(0, len(a), 2):
    popval = a.pop(i)
    a.insert(i + 1, popval)

print(a)