"""
Given a list of integers,
find the first maximum element in it. Print its value and its index (counting with 0).
"""
# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)

M=max(a)
indice=a.index(M)
print(M,indice, end=" ")
