"""
Given a list of non-zero integers,
find and print the first adjacent pair of elements that have the same sign.
If there is no such pair, print 0.
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)

for i in range(1, len(a)):
  if a[i] * a[i-1] > 0:
    print(str(a[i - 1]), str(a[i]))
    break
  elif i == len(a)-1:
    print("0")
