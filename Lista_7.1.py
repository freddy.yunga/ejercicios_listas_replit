#Given a list of numbers, find and print all its elements with even indices (i.e. A[0], A[2], A[4], ...).

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
#print(a)
#a=[2,3,4,5,6,78]
#cant=int(input(" cuantos numeros: "))
indice=0
for i in a:
  if indice % 2==0:
    print(i, end=" ")
  indice=indice+1
