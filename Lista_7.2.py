"""
Given a list of numbers, print all its even elements.
Use a for-loop that iterates over the list itself and not over its indices.
That is, don't use range()
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
for s in a:
    if s % 2 == 0:
        print(s, end=" ")
