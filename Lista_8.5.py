"""
Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
followed by two non-negative integers i and j less than n,
swap the columns i and j of 2d list and print the result.

"""
# Read a 2D list of integers:
#a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)


m, n = [int(k) for k in input().split()]
a = [[int(k) for k in input().split()] for i in range(m)]


for i in range(m):
  for j in range(n):
    a[i][j]=a[i][j]*1
print('\n'.join([' '.join([str(k) for k in row]) for row in a]))
