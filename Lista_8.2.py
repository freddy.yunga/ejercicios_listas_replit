"""
Given two integers - the number of rows m and columns n of m×n 2d list - and subsequent m rows of n integers,
find the maximal element and print its row number and column number.
If there are many maximal elements in different rows, report the one with smaller row number.
If there are many maximal elements in the same row, report the one with smaller column number.
"""

# Read a 2D list of integers:
D = input().split()
NUM_ROWS = int(D[0])
NUM_COLS = int(D[1])
# NUM_COLS=4
a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]

# Print a value:
# print(a)
# c=int(input())
mayor = 0
for ci in range(NUM_ROWS):
    for cj in range(NUM_COLS):
        # a[ci][cj]=a[ci][cj]*c
        if a[ci][cj] > mayor:
            mayor = a[ci][cj]

print(mayor)

"""for fila in a:
  for columna in fila:
    print(columna, end=" ")
  print()
"""