"""
Given a list of numbers with all elements sorted in ascending order,
determine and print the number of distinct elements in it.
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
"""
cont=0
cont2=0
for s in a:
  if a[s]==a[s]:
    cont=cont+1
print(cont)
"""

total = 1
for i in range(1, len(a)):
    if a[i - 1] != a[i]:
        total += 1

print(total)