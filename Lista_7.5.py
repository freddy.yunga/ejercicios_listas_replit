"""
Given a list of numbers, determine and print the number of elements that are greater than both of their neighbors.

The first and the last items of the list shouldn't be considered because they don't have two neighbors.
"""

# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)

elements = 0
for i in range(1, len(a) - 1):
  if a[i - 1] < a[i] > a[i + 1]:
    elements += 1
print(elements)