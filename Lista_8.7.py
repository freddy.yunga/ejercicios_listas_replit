"""
Given two positive integers n and m,
create a two-dimensional array of size n×m and populate it with the characters "."and "*" in a chequered pattern.
The top left corner should have the character "." .

"""
# Read a list of integers:
a = [int(s) for s in input().split()]
#a=int(input())
# Print a value:
# print(a)

b = [['.'] * a for i in range(a)]
for i in range(a):
  b[i][i] = '*'
  b[a // 2][i] = '*'
  b[i][a // 2] = '*'
  b[i][a - i - 1] = '*'
for row in b:
  print(' '.join(row))